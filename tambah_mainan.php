<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="widt h=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Tambah Menu</h2>
  <form action="input_mainan.php" method="POST">
    <div class="form-group">
      <label for="email">ID mainan:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID Mainan" name="id_mainan">
    </div>
    <div class="form-group">
      <label for="email">Nama mainan:</label>
      <input type="text" class="form-control" placeholder="Masukkan Nama Makanan" name="nama_mainan">
    </div>
    <div class="form-group">
      <label for="pwd">Harga:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Harga" name="harga_mainan">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

</body>
</html>
