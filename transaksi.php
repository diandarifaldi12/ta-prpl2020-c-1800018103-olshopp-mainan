<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Appetizer - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Miss+Fajardose&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="py-1 bg-black top">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
	    		<div class="col-lg-12 d-block">
		    		<div class="row d-flex">
		    			<div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <span class="text">+628 4567 1234 </span>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
						    <span class="text">tokomainanajip@gmail.com</span>
					    </div>
					    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right justify-content-end">
					    </div>
				    </div>
			    </div>
		    </div>
		  </div>
    </div>
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="mainan.php">Transaksi</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	        	<li class="nav-item"><a href="mainan.php" class="nav-link">Home</a></li>
	        	<li class="nav-item"><a href="about.html" class="nav-link">Tentang</a></li>
	        	<li class="nav-item"><a href="menu.php" class="nav-link">Menu</a></li>
	        	<li class="nav-item"><a href="lihat_pelanggan.php" class="nav-link">Pelanggan</a></li>
	          <li class="nav-item"><a href="karyawan1.php" class="nav-link">Karyawan</a></li>
	          <li class="nav-item cta"><a href="transaksi.php" class="nav-link">Transaksi</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->
    
    <section class="hero-wrap hero-wrap-2" style="background-image: url('images/tf.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">Transaksi</h1>
          </div>
        </div>
      </div>
    </section>

<?php include 'koneksi.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Form Transaksi</h2>
  <form action="" method="POST">
    <div class="form-group">
      <label for="email">ID pembelian:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID pembelian" name="id_pembelian">
    </div>
    <div class="form-group">
      <label for="pwd">ID mainan:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID mainan" name="id_mainan">
    </div>
    <div class="form-group">
      <label for="pwd">ID Pelanggan:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID Pelanggan" name="id_pelanggan">
    </div>
    <div class="form-group">
      <label for="pwd">Jumlah Barang:</label>
      <input type="text" class="form-control" placeholder="Masukkan Jumlah Barang" name="jmlh_brng">
    </div>
    <div class="form-group">
      <label for="pwd">Total :</label>
      <input type="text" class="form-control" placeholder="Masukkan Total" name="total_jajan">
    </div>
    <div class="form-group">
      <label for="pwd">Tanggal Beli:</label>
      <input type="date" class="form-control" placeholder="Masukkan Tanggal Pembelian" name="tanggal_beli">
    </div>
    <div class="form-group">
      <label for="pwd">Alamat:</label>
      <input type="text" class="form-control" placeholder="Masukkan Alamat" name="alamat">
    </div>
    <div class="form-group">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

<?php 

if($_SERVER['REQUEST_METHOD']=='POST'){
  include 'koneksi.php';
$id_pembelian= $_POST['id_pembelian'];
$id_mainan= $_POST['id_mainan'];
$id_pelanggan= $_POST['id_pelanggan'];
$jmlh_brng= $_POST['jmlh_brng'];
$total_jajan= $_POST['total_jajan'];
$tanggal_beli= $_POST['tanggal_beli'];
$alamat= $_POST['alamat'];

  if($id_pembelian=='' || $id_mainan == '') {
    echo "Form belum lengkap!!!";
  }else if($id_pelanggan=='' || $jmlh_brng == '') {
    echo "Form belum lengkap!!!";
  }else if($total_jajan=='' || $tanggal_beli == '') {
    echo "Form belum lengkap!!!";
  }else if($alamat=='') {
    echo "Form belum lengkap!!!";
  }else{
    $simpan = mysqli_query($koneksi, "INSERT INTO data_transaksi(id_pembelian,id_mainan,id_pelanggan,jmlh_brng,total_jajan,tanggal_beli,alamat) VALUES('$id_pembelian','$id_mainan','$id_pelanggan','$jmlh_brng','$total_jajan','$tanggal_beli','$alamat')");
    if (!$simpan) {
      echo "Simpan data gagal!!";
    }else{
      header('location:tampil_transaksi.php');
    }
  }
}
?>

</body>
</html>
  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>