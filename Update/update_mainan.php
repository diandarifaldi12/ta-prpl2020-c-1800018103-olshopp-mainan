  <?php 

  include 'koneksi.php';
  $id_mainan = $_GET['id_mainan'];
  $data = mysqli_query($koneksi, "SELECT *FROM data_mainan WHERE id_mainan='$id_mainan'");
  ?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  </head>
  <body>

  <div class="container">
    <h2>Update data mainan </h2>
    <form action="proses_updatemainan.php" method="POST">
    <?php foreach ($data as $value): ?>
      <div class="form-group">
        <label for="email">ID Mainan:</label>
        <input type="text" class="form-control" id="email" value="<?php echo $value['id_mainan'] ?>" name="id_mainan" readonly>
      </div>
      <div class="form-group">
        <label for="pwd">Nama Mainan:</label>
        <input type="text" class="form-control" id="pwd" value="<?php echo $value['nama_mainan'] ?>" name="nama_mainan">
      </div>
      <div class="form-group">
        <label for="pwd">Harga Mainan:</label>
        <input type="text" class="form-control" id="pwd" value="<?php echo $value['harga_mainan'] ?>" name="harga_mainan">
      </div>
      <div class="form-group">
      </div>
      <div class="form-group form-check">
        <label class="form-check-label">
          <input class="form-check-input" type="checkbox" name="remember"> Remember me
        </label>
      </div>
  <?php endforeach ?>

      <button type="submit" class="btn btn-primary">Update</button>
    </form>
  </div>

  </body>
  </html>