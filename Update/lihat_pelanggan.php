<!DOCTYPE html>
<html lang="en">
  <head>
    <title>TOKO MAINAN AJIP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Miss+Fajardose&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="py-1 bg-black top">
      <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
          <div class="col-lg-12 d-block">
            <div class="row d-flex">
              <div class="col-md pr-4 d-flex topper align-items-center">
                <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
                <span class="text">+628 4567 1234</span>
              </div>
              <div class="col-md pr-4 d-flex topper align-items-center">
                <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
                <span class="text">tokomainanajip@gmail.com</span>
              </div>
              <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right justify-content-end">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="mainan.php">DATA PELANGGAN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="mainan.php" class="nav-link">Home</a></li>
            <li class="nav-item"><a href="about.html" class="nav-link">Tentang</a></li>
            <li class="nav-item"><a href="menu.php" class="nav-link">Menu</a></li>
            <li class="nav-item"><a href="lihat_pelanggan.php" class="nav-link">Pelanggan</a></li>
            <li class="nav-item"><a href="karyawan1.php" class="nav-link">Karyawan</a></li>
            <li class="nav-item cta"><a href="transaksi.php" class="nav-link">Transaksi</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->
    
    <section class="home-slider owl-carousel js-fullheight">
      <div class="slider-item js-fullheight" style="background-image: url(images/bg1.jpg);">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text js-fullheight justify-content-center align-items-center" data-scrollax-parent="true">

            <div class="col-md-12 col-sm-12 text-center ftco-animate">
              <h1 class="mb-4 mt-5">SELAMAT DATANG</h1>
            </div>

          </div>
        </div>
      </div>

      <div class="slider-item js-fullheight" style="background-image: url(images/bg2.jpg);">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text js-fullheight justify-content-center align-items-center" data-scrollax-parent="true">

            <div class="col-md-12 col-sm-12 text-center ftco-animate">
              <h1 class="mb-4 mt-5">DI</h1>
            </div>

          </div>
        </div>
      </div>

      <div class="slider-item js-fullheight" style="background-image: url(images/bg3.jpg);">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

            <div class="col-md-12 col-sm-12 text-center ftco-animate">
              <h1 class="mb-4 mt-5">TOKO MAINAN AJIP</h1>
            </div>

          </div>
        </div>
      </div>
    </section>
  
    <section class="ftco-section ftco-wrap-about ftco-no-pb">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-sm-10 wrap-about ftco-animate text-center">
            <div class="heading-section mb-4 text-center">
              
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(images/bg_4.jpg);" data-stellar-background-ratio="0.5">
    <!-- <section class="ftco-section ftco-counter img ftco-no-pt" id="section-counter"> -->
      <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="text">
                    <strong class="number" data-number="1000">0</strong>
                    <span>Pelanggan yang menyukai</span>
                  </div>
                </div>
              </div>
              
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="ftco-section">
      <div class="container-fluid px-4">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">Data Pelanggan</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 menu-wrap">
            <div class="heading-menu text-center ftco-animate">
            </div>
          <!--  -->
    <form method="post" enctype="multipart/form-data">
            <div class="form-group row mt-2">
              <div class="col-sm-10">
                <input type="text" name="keyword" class="form-control" id="">
              </div>
        <button type="submit" class="btn btn-sm btn-success" name="cari">Cari</button>
            </div>
    </form>
<?php 
include 'koneksi.php';
$data = mysqli_query($koneksi, "SELECT * FROM data_pelanggan");
 ?>
 <center>
<table border="3">
  <tr>
    <td>ID Pelanggan</td>
    <td>ID Mainan</td>
    <td>Nama Pelanggan</td>
    <td>Jumlah </td>
    <td>ID Karyawan</td>
    <td>action</td>
  </tr>

  <?php foreach($data as $value): ?>
  <tr>
    <td><h3> <?php echo $value['id_pelanggan']; ?> </h3></td>
    <td><h3> <?php echo $value['id_mainan']; ?> </h3> </td>
    <td><h3> <?php echo $value['nama_pelanggan']; ?> </h3> </td>  
    <td><h3> <?php echo $value['jumlah']; ?> </h3></td>
    <td><h3> <?php echo $value['id_karyawan']; ?> </h3></td>
    <td> 
      <a href="hapus_pelanggan.php?id_pelanggan=<?php echo $value ['id_pelanggan'] ?>"> Hapus </a> 
      <a href="update_pelanggan.php?id_pelanggan=<?php echo $value ['id_pelanggan'] ?>"> Update </a>
    </td>

  </tr>
  <?php endforeach ?>
  <p></p>
  <a href="tambah_pelanggan.php">Tambah data </a>
</table>
</center>
<br>
<p align="left"><a href="pencarian_mainan.php"><li>Mencari Data Mainan dengan nama</li></a></p>
<p align="left"><a href="pencarian_pelanggan.php"><li>Mencari data pelanggan dengan nama</li></a></p>
</br>
            

  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>