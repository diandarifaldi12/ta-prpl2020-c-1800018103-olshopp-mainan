<?php 

include 'koneksi.php';
$id_pelanggan = $_GET['id_pelanggan'];
$data = mysqli_query($koneksi, "SELECT *FROM data_pelanggan WHERE id_pelanggan='$id_pelanggan'");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Edit Data Pelanggan</h2>
  <form action="proses_update_pelanggan.php" method="POST">
  <?php foreach ($data as $value): ?>
    <div class="form-group">
      <label for="email">ID pelanggan:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['id_pelanggan'] ?>" name="id_pelanggan" readonly>
    </div>
    <div class="form-group">
      <label for="email">ID Mainan:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['id_mainan'] ?>" name="id_mainan" readonly>
    </div>
    <div class="form-group">
      <label for="pwd">Nama Pelanggan:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['nama_pelanggan'] ?>" name="nama_pelanggan">
    </div>
    <div class="form-group">
      <label for="pwd">Jumlah:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['jumlah'] ?>" name="jumlah">
    </div>
    <div class="form-group">
      <label for="pwd">ID Karyawan yang Melayani:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['id_karyawan'] ?>" name="id_karyawan">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
<?php endforeach ?>

    <button type="submit" class="btn btn-primary">Update</button>
  </form>
</div>

</body>
</html>
