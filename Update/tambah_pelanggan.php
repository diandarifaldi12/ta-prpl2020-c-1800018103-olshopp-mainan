<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="widt h=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Tambah Data Pelanggan</h2>
  <form action="input_pelanggan.php" method="POST">
    <div class="form-group">
      <label for="email">ID Pelanggan:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID Pelanggan" name="id_pelanggan">
    </div>
    <div class="form-group">
      <label for="email">ID Mainan:</label>
      <input type="text" class="form-control" placeholder="Masukkan ID Mainan" name="id_mainan">
    </div>
    <div class="form-group">
      <label for="email">Nama Pelanggan:</label>
      <input type="text" class="form-control" placeholder="Masukkan Nama Pelanggan" name="nama_pelanggan">
    </div>
    <div class="form-group">
      <label for="pwd">Jumlah:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan Jumlah" name="jumlah">
    </div>
    <div class="form-group">
      <label for="pwd">ID Karyawan yang Melayani:</label>
      <input type="text" class="form-control" id="pwd" placeholder="Masukkan ID Karyawan" name="id_karyawan">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>

</body>
</html>
