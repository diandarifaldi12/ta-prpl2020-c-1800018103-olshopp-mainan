<?php 

include 'koneksi.php';
$id_karyawan = $_GET['id_karyawan'];
$data = mysqli_query($koneksi, "SELECT *FROM data_karyawan WHERE id_karyawan='$id_karyawan'");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Edit Karyawan</h2>
  <form action="proses_updatekaryawan.php" method="POST">
  <?php foreach ($data as $value): ?>
    <div class="form-group">
      <label for="email">ID Karyawan:</label>
      <input type="text" class="form-control" id="email" value="<?php echo $value['id_karyawan'] ?>" name="id_karyawan" readonly>
    </div>
    <div class="form-group">
      <label for="pwd">Nama:</label>
      <input type="text" class="form-control" id="pwd" value="<?php echo $value['nama_karyawan'] ?>" name="nama_karyawan">
    </div>
    <div class="form-group">
      <label for="pwd">Tempat & Tanggal Lahir:</label>
      <input type="date" class="form-control" id="pwd" value="<?php echo $value['TTL'] ?>" name="TTL">
    </div>
    <div class="form-group">
      <label for="email">Jenis Kelamin:</label>
      <p><input type="radio" name="jenis_kelamin" value="Laki-Laki"/>Laki-laki</p>
      <p><input type="radio" name="jenis_kelamin" value="Perempuan"/>Perempuan</p>
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
<?php endforeach ?>

    <button type="submit" class="btn btn-primary">Update</button>
  </form>
</div>

</body>
</html>
