<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Restoran Enak Bae</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Miss+Fajardose&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="py-1 bg-black top">
      <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
          <div class="col-lg-12 d-block">
            <div class="row d-flex">
              <div class="col-md pr-4 d-flex topper align-items-center">
                <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
                <span class="text">+6285600511220</span>
              </div>
              <div class="col-md pr-4 d-flex topper align-items-center">
                <div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
                <span class="text">Lastio1800018115@webmail.uad.ac.id</span>
              </div>
              <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right justify-content-end">
                <p class="mb-0 register-link"><span>Buka : </span> <span>Senin - Sabtu</span> <span>8:00 Pagi - 9:00 Malam</span></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
        <a class="navbar-brand" href="index.html">Restoran Enak Bae</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active"><a href="index.html" class="nav-link">Home</a></li>
            <li class="nav-item"><a href="about.html" class="nav-link">Tentang</a></li>
            <li class="nav-item"><a href="menu1.php" class="nav-link">Menu</a></li>
            <li class="nav-item active"><a href="pembelian1.php" class="nav-link">pembelian</a></li>
            <li class="nav-item"><a href="contact.html" class="nav-link">Kontak Informasi</a></li>
            <li class="nav-item cta"><a href="karyawan1.php" class="nav-link">Karyawan</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->
    
    <section class="home-slider owl-carousel js-fullheight">
      <div class="slider-item js-fullheight" style="background-image: url(images/bg1.jpg);">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text js-fullheight justify-content-center align-items-center" data-scrollax-parent="true">

            <div class="col-md-12 col-sm-12 text-center ftco-animate">
              <h1 class="mb-4 mt-5">Apa Bae Enak</h1>
              <p><a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3">Pesan Sekarang!</a> <a href="#" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Lihat Menu</a></p>
            </div>

          </div>
        </div>
      </div>

      <div class="slider-item js-fullheight" style="background-image: url(images/bg2.jpg);">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text js-fullheight justify-content-center align-items-center" data-scrollax-parent="true">

            <div class="col-md-12 col-sm-12 text-center ftco-animate">
              <h1 class="mb-4 mt-5">Ora Ngandel Jajal </h1>
              <p><a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3">Pesan Sekarang!</a> <a href="#" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Lihat Menu</a></p>
            </div>

          </div>
        </div>
      </div>

      <div class="slider-item js-fullheight" style="background-image: url(images/bg3.jpg);">
        <div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

            <div class="col-md-12 col-sm-12 text-center ftco-animate">
              <h1 class="mb-4 mt-5">Enak Loh</h1>
              <p><a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3">Pesan Sekarang</a> <a href="#" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Lihat Menu</a></p>
            </div>

          </div>
        </div>
      </div>
    </section>
  

    <section class="ftco-section ftco-wrap-about ftco-no-pb">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-sm-10 wrap-about ftco-animate text-center">
            <div class="heading-section mb-4 text-center">
              <h2 class="mb-4">Restoran Enak Bae</h2>
            </div>
            <p>Menyediakan Berbagai jenis makanan seperti Appetizer, Main Course dan Dessert dengan harga yang murah!</p>

            <div class="video justify-content-center">
              <a href="https://vimeo.com/45830194" class="icon-video popup-vimeo d-flex justify-content-center align-items-center">
                <span class="ion-ios-play"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>

    
    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(images/bg4.jpg);" data-stellar-background-ratio="0.5">
    <!-- <section class="ftco-section ftco-counter img ftco-no-pt" id="section-counter"> -->
      <div class="container">
        <div class="row d-md-flex align-items-center justify-content-center">
          <div class="col-lg-10">
            <div class="row d-md-flex align-items-center">
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="text">
                    <strong class="number" data-number="1">0</strong>
                    <span>Berdiri Tahun</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="text">
                    <strong class="number" data-number="300">0</strong>
                    <span>Jumlah Pelanggan</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="text">
                    <strong class="number" data-number="15">0</strong>
                    <span>Menu</span>
                  </div>
                </div>
              </div>
              <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                <div class="block-18">
                  <div class="text">
                    <strong class="number" data-number="5">0</strong>
                    <span>Karyawan</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">Melayani Katering</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
            <div class="media block-6 services d-block">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-cake"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Ulang Tahun</h3>
              </div>
            </div>      
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
            <div class="media block-6 services d-block">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-meeting"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Acara Kantor</h3>
              </div>
            </div>    
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate text-center">
            <div class="media block-6 services d-block">
              <div class="icon d-flex justify-content-center align-items-center">
                <span class="flaticon-tray"></span>
              </div>
              <div class="media-body p-2 mt-3">
                <h3 class="heading">Pernikahan</h3>
              </div>
            </div>      
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
      <div class="container-fluid px-4">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-7 text-center heading-section ftco-animate">
            <h2 class="mb-4">Menu</h2>
            <form method="post" enctype="multipart/form-data">
            <div class="form-group row mt-2">
                    <div class="col-sm-10">
                      <input type="text" name="keyword" class="form-control" id="">
                    </div>
                    <button type="submit" class="btn btn-sm btn-success" name="cari">Cari</button>
                  </div>
            </form>
<center>
<table border="3">
  <tr>
    <td>ID Pembelian</td>
    <td>ID Makanan</td>
    <td>Tanggal</td>
    <td>Jumlah Pembelian</td>
    <td>Total Pembelian</td>
    <td>ID Karyawan</td>
    <td>action</td>
  </tr>

 <?php 
include 'koneksi.php';
if(isset($_POST["cari"])){
            $search = $_POST['keyword'];

            $query = $koneksi->query("SELECT * FROM data_pembelian WHERE id_pembelian LIKE '%$search%'");
                            } else {
                                $query = $koneksi->query("SELECT * FROM data_pembelian");
                            }

                            $no = 1;

                            while($value = mysqli_fetch_assoc($query)) {
    ?>
  <tr>
    <td><h3> <?php echo $value['id_pembelian']; ?> </h3></td>
    <td><h3> <?php echo $value['id_makanan']; ?> </h3> </td>
    <td><h3> <?php echo $value['tanggal']; ?> </h3></td>
    <td><h3> <?php echo $value['jumlah_pembelian']; ?> </h3></td>
    <td><h3> <?php echo $value['total_pembelian']; ?> </h3></td>
    <td><h3> <?php echo $value['id_karyawan']; ?> </h3></td>
    <td> 
      <a href="hapus_pembelian.php?id_pembelian=<?php echo $value ['id_pembelian'] ?>"> Hapus </a> 
      <a href="update_pembelian.php?id_pembelian=<?php echo $value ['id_pembelian'] ?>"> Update </a>
    </td>

  </tr>
  <?php } ?>
  <p></p>
  <a href="tambah_pembelian.php">Tambah data </a>
</table>
<br>
    <p align="left"><a href="menu1.php"><li>Mencari Data Makanan</li></a></p>
    <p align="left"><a href="pembelian1.php"><li>Mencari Data Pembelian</li></a></p>
</center>
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>